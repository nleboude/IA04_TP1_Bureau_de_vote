package comsoc

import (
	"errors"
	"fmt"
	"math/rand"
	"strconv"

	"golang.org/x/exp/slices"
	"gonum.org/v1/gonum/stat/combin"
)

//Borda : dernier 0 pt et premier m-1 points
//var borda = []Int{1, 2, 3}
// on pourrait faire une fct qui calculerait les points si autre méthode

var Patate = "patateee"

func BordaSWF(p Profile) (count Count, err error) {

	err = checkProfile(p, alt)
	if err != nil {
		return count, err
	}

	//initalisation, alternatives n'ont pas encore de score
	count = make(Count)
	for _, i := range alt {
		count[i] = 0
	}

	for _, votant := range p {
		fmt.Println(votant)
		for i, v := range votant { //sur le classement du votant, on donne des points
			count[v] += len(alt) - 1 - i
		}
	}

	return count, err
}

func BordaSCF(p Profile) (bestAlts []Alternative, err error) {

	decompte, err := BordaSWF(p)
	if err != nil {
		fmt.Println("erreur BordaSWF")
		return bestAlts, err
	}

	fmt.Println(decompte)
	bestAlts = maxCount(decompte)

	return bestAlts, err
}

func CombiAlt(alts []Alternative) (comb [][]Alternative) {

	combats := combin.Combinations(len(alt), 2)
	var com []Alternative
	for _, duel := range combats {
		com = nil
		for _, i := range duel {
			com = append(com, alts[i])
		}
		comb = append(comb, com)
	}

	return comb
}

func CondorcetWinner(p Profile) (bestAlts []Alternative, err error) {

	err = checkProfile(p, alt)
	if err != nil {
		return bestAlts, err
	}

	combats := CombiAlt(alt)
	// for _, duel := range combats {
	// 	fmt.Println("duel", duel)
	// }

	//initalisation, alternatives n'ont pas encore combattu
	resmatch := make(Count)
	for _, score := range alt {
		resmatch[score] = 0
	}
	//fmt.Println("avant", resmatch)

	//on fait chaque duel
	for _, duel := range combats { // on fait tous les duels pour chaque profil
		scoreA := 0
		scoreB := 0
		//fmt.Println("duel en cours :", duel)
		for _, pref := range p {
			//fmt.Println("pref en cours:", pref)
			if isPref(duel[0], duel[1], pref) {
				scoreA += 1
				//fmt.Println("gagnant A")
			} else {
				scoreB += 1
				//fmt.Println("gagnant B")
			}
		}

		if scoreA > scoreB {
			resmatch[duel[0]] += 1
			//fmt.Println("scoreA > scoreB")
		} else if scoreA < scoreB {
			resmatch[duel[1]] += 1
			//fmt.Println("scoreA < scoreB")
		} else { //égalite tiebreak
			var ega = []Alternative{duel[0], duel[1]}
			winner, _ := TieBreak(ega)
			//fmt.Println("tiebreak winner:", winner)
			resmatch[winner] += 1
		}
	}

	//fmt.Println(resmatch)
	//on analyse les scores
	for gladiator, score := range resmatch {
		if score == len(alt)-1 {
			bestAlts = append(bestAlts, gladiator)
			return bestAlts, err
		}
	}

	return bestAlts, err
}

//bien etre social : décompte à partir d'un profil
//func SWF(p Profile) (count Count, err error)
//choix social : alternatives préférees
//func SCF(p Profile) (bestAlts []Alternative, err error)

func MajoritySWF(p Profile) (count Count, err error) {

	err = checkProfile(p, alt)
	if err != nil {
		return count, err
	}

	//initalisation, alternatives n'ont pas encore de voix
	count = make(Count)
	for _, i := range alt {
		count[i] = 0
	}

	for _, votant := range p {
		fmt.Println(votant)
		count[votant[0]] += 1 //0 pcq on s'interresse qu'au chouchou du votant
	}

	return count, err
}

func MajoritySCF(p Profile) (bestAlts []Alternative, err error) {

	decompte, err := MajoritySWF(p)
	if err != nil {
		fmt.Println("erreur MajoritySWF")
		return bestAlts, err
	}

	bestAlts = maxCount(decompte)

	return bestAlts, err
}

// ega = alternatives à départager
func TieBreak(ega []Alternative) (win Alternative, err error) {

	tirage := rand.Intn(len(ega))
	win = ega[tirage]

	return win, err
}

// Les alternatives sont representees par des entiers.
type Alternative int

// profile[12]: preferences du votant 12, +aimé à -aimé
type Profile [][]Alternative

// Décompte associant à chaque alternative à un score
type Count map[Alternative]int

var alt = []Alternative{1, 2, 3}

// renvoie l'indice ou se trouve alt dans prefs
func rank(alt Alternative, prefs []Alternative) int {
	for i, v := range prefs {
		if alt == v {
			//fmt.Println(i)
			return i
		}
	}
	return -1
}

// renvoie vrai ssi alt1 est préférée à alt2
func isPref(alt1, alt2 Alternative, prefs []Alternative) bool {
	if rank(alt1, prefs) < rank(alt2, prefs) {
		return true
	}
	return false
}

// renvoie les meilleures alternatives pour un décomtpe donné
// faut renvoyer le meilleur ou un classement trié ?
func maxCount(count Count) (bestAlts []Alternative) {
	// bestAlts = append(bestAlts, Alternative(count[0]))

	for key, score := range count {
		if bestAlts == nil { //initalisation
			bestAlts = append(bestAlts, key)
		} else if score == count[bestAlts[0]] { //si égalité entre alternatives
			bestAlts = append(bestAlts, key)
		} else if score > count[bestAlts[0]] { //si meilleur que current
			bestAlts = nil
			bestAlts = append(bestAlts, key)
		}
		//fmt.Println("bestAlts :", bestAlts)
	}
	return bestAlts
}

func OnlyOnce(prefs []Alternative) bool {

	var in []Alternative
	for _, alt := range prefs {
		if slices.Contains(in, alt) { // alt deja vu
			return false
		} else { //premier fois qu'on voit alt
			in = append(in, alt)
		}
	}
	return true
}

// verifie le profil donné, complets, une seule occurrence etc..
func checkProfile(prefs Profile, alt []Alternative) error {
	for p, v := range prefs {
		//fmt.Println(v)
		if len(v) == 0 { //profile vide
			err := "erreur : profile [" + strconv.Itoa(p) + "] vide"
			return errors.New(err)
		}
		if len(v) != len(alt) { //profile incomplet
			err := "erreur : profile [" + strconv.Itoa(p) + "] incomplet"
			return errors.New(err)
		}
		if !OnlyOnce(v) {
			err := "erreur : profile [" + strconv.Itoa(p) + "] contient plusieurs fois la meme alternative"
			return errors.New(err)
		}

	}
	return nil // nil == no error
}

func ApprovalSWF(p Profile, thresholds []int) (count Count, err error) {

	err = checkProfile(p, alt)
	if err != nil {
		return count, err
	}

	//initalisation, alternatives n'ont pas encore de points
	count = make(Count)
	for _, i := range alt {
		count[i] = 0
	}

	for i, pref := range p {
		for j := 0; j < thresholds[i]; j++ {
			count[pref[j]] += 1
		}

	}
	return count, err
}

func ApprovalSCF(p Profile, thresholds []int) (bestAlts []Alternative, err error) {

	decompte, err := ApprovalSWF(p, thresholds)
	if err != nil {
		fmt.Println("erreur ApprovalSWF")
		return bestAlts, err
	}

	bestAlts = maxCount(decompte)

	return bestAlts, err

}

func Copeland(p Profile) (bestAlts []Alternative, err error) {

	err = checkProfile(p, alt)
	if err != nil {
		return bestAlts, err
	}

	combats := CombiAlt(alt)
	// for _, duel := range combats {
	// 	fmt.Println("duel", duel)
	// }

	//initalisation, alternatives n'ont pas encore combattu
	resmatch := make(Count)
	for _, score := range alt {
		resmatch[score] = 0
	}
	//fmt.Println("avant", resmatch)

	//on fait chaque duel
	for _, duel := range combats { // on fait tous les duels pour chaque profil
		scoreA := 0
		scoreB := 0
		//fmt.Println("duel en cours :", duel)
		for _, pref := range p {
			//fmt.Println("pref en cours:", pref)
			if isPref(duel[0], duel[1], pref) {
				scoreA += 1
				//fmt.Println("gagnant A")
			} else {
				scoreB += 1
				//fmt.Println("gagnant B")
			}
		}

		if scoreA > scoreB {
			resmatch[duel[0]] += 1
			resmatch[duel[1]] -= 1
			//fmt.Println("scoreA > scoreB")
		} else if scoreA < scoreB {
			resmatch[duel[1]] += 1
			resmatch[duel[0]] -= 1
			//fmt.Println("scoreA < scoreB")
		} else { //égalite tiebreak
			var ega = []Alternative{duel[0], duel[1]}
			winner, _ := TieBreak(ega)
			//fmt.Println("tiebreak winner:", winner)
			resmatch[winner] += 1
			if winner == duel[0] {
				resmatch[duel[1]] -= 1
			} else {
				resmatch[duel[0]] -= 1
			}
		}
	}

	//fmt.Println(resmatch)
	//on analyse les scores
	bestAlts = maxCount(resmatch)
	return bestAlts, err
}
