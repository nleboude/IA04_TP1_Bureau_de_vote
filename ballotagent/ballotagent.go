package ballotagent

import (
	"tp1/comsoc"
)

type NewBallot_Request struct {
	ID        string   `json:"vote_id"`
	Rule      string   `json:"rule"`
	Deadline  string   `json:"deadline"`
	Voter_ids []string `json:"voter_Ids"`
	Alts      int      `json:"alts"`
}

type NewResult_Request struct {
	Ballot_Id string `json:"ballot_Id"`
}

type NewBallot_Response struct {
	Ballot_ID string `json:"New Ballot"`
}

type Vote_Request struct {
	Agent_ID string               `json:"agent_id"`
	Vote_ID  string               `json:"vote_id"`
	Prefs    []comsoc.Alternative `json:"prefs"`
	Options  []int                `json:"options"`
}
