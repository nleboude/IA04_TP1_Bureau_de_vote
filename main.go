package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"html"
	"log"
	"net/http"
	"os"
	"strconv"
	"sync"
	"time"
	"tp1/ballotagent"
	"tp1/comsoc"
)

type MyServer struct {
	sync.Mutex
	Profile      comsoc.Profile
	ID           int
	Ballotagents map[string]*RestClientAgent
}

type Ballotagent struct {
	id       string
	rule     string
	deadline string
	voterIds map[string]bool
	options  []int
	nbAlts   int
	bestAlts []comsoc.Alternative
	profil   comsoc.Profile
}

func (s *MyServer) VoteRequest(w http.ResponseWriter, r *http.Request) {
	s.Lock()
	defer s.Unlock()
	fmt.Fprintf(w, "VoteRequest, %q",
		html.EscapeString(r.URL.Path))

	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)

	var re ballotagent.Vote_Request
	json.Unmarshal(buf.Bytes(), &re)
	ballot, _ := s.Ballotagents[re.Vote_ID]

	if ballot.voterIds[re.Agent_ID] {
		fmt.Println("Cet agent a déjà voté.")
		// w.WriteHeader(http.StatusBadRequest)
		// fmt.Fprint(w)
	} else {
		ballot.profil = append(ballot.profil, re.Prefs)
		ballot.voterIds[re.Agent_ID] = true
		log.Println("vote pris en compte : ", re.Prefs)
	}
}

func NewRestBallotAgent(vote_id string, votingMethod string, alts int, deadline string, voter_ids []string) *Ballotagent {
	profil := make(comsoc.Profile, 0)
	bestAlts := make([]comsoc.Alternative, 0)
	voterIds := make(map[string]bool)
	options := make([]int, 0)
	for _, voter_id := range voter_ids {
		voterIds[voter_id] = false
	}
	return &Ballotagent{
		id:       vote_id,
		rule:     votingMethod,
		deadline: deadline,
		voterIds: voterIds,
		options:  options,
		nbAlts:   alts,
		bestAlts: bestAlts,
		profil:   profil,
	}
}

type RestClientAgent struct {
	rule     string
	id       string
	deadline string
	voterIds map[string]bool
	nbAlts   int
	options  []int
	bestAlts []comsoc.Alternative
	profil   comsoc.Profile
}

func NewRestClientAgent(id string, rule string, deadline string, nbAlts int, voter_ids []string) *RestClientAgent {
	profil := make(comsoc.Profile, 0)
	bestAlts := make([]comsoc.Alternative, 0)
	voterIds := make(map[string]bool)
	options := make([]int, 0)
	for _, voter_id := range voter_ids {
		voterIds[voter_id] = false
	}
	return &RestClientAgent{rule, id, deadline, voterIds, nbAlts, options, bestAlts, profil}
}

func (s *MyServer) NewBallot(w http.ResponseWriter, r *http.Request) {
	s.Lock()
	defer s.Unlock()
	fmt.Fprintf(w, "NewBallot, %q",
		html.EscapeString(r.URL.Path))

	var re ballotagent.NewBallot_Request

	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)

	json.Unmarshal(buf.Bytes(), &re)

	id := "ag_id"
	id += strconv.Itoa(0)

	new_ballot_agent := NewRestClientAgent(re.ID, re.Rule, re.Deadline, re.Alts, re.Voter_ids)

	if s.Ballotagents[re.ID] == nil {
		s.Ballotagents = map[string]*RestClientAgent{}
		s.Ballotagents[re.ID] = new_ballot_agent
	} else {
		s.Ballotagents[re.ID] = new_ballot_agent
	}

	msg, _ := json.Marshal(ballotagent.NewBallot_Response{Ballot_ID: re.ID})
	w.Write(msg)

	log.Println("Creation d'un ballot : ", re.ID)
}

func (s *MyServer) ResultRequest(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "ResultRequest, %q",
		html.EscapeString(r.URL.Path))
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)

	var result ballotagent.NewResult_Request
	var err error
	json.Unmarshal(buf.Bytes(), &result)

	switch s.Ballotagents[result.Ballot_Id].rule {
	case "Majority":
		s.Ballotagents[result.Ballot_Id].bestAlts, err = comsoc.MajoritySCF(s.Ballotagents[result.Ballot_Id].profil)
	case "Borda":
		s.Ballotagents[result.Ballot_Id].bestAlts, err = comsoc.BordaSCF(s.Ballotagents[result.Ballot_Id].profil)
	case "Approval":
		s.Ballotagents[result.Ballot_Id].bestAlts, err = comsoc.ApprovalSCF(s.Ballotagents[result.Ballot_Id].profil, s.Ballotagents[result.Ballot_Id].options)
	case "Condorcet":
		s.Ballotagents[result.Ballot_Id].bestAlts, err = comsoc.CondorcetWinner(s.Ballotagents[result.Ballot_Id].profil)
	case "Copeland":
		s.Ballotagents[result.Ballot_Id].bestAlts, err = comsoc.Copeland(s.Ballotagents[result.Ballot_Id].profil)
	default:
		fmt.Println("Methode de vote inconnue.")
	}

	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR :  %v\n", err)
		os.Exit(1)
	}

	if s.Ballotagents[result.Ballot_Id].bestAlts != nil {
		fmt.Println(s.Ballotagents[result.Ballot_Id].bestAlts)
		winner := int(s.Ballotagents[result.Ballot_Id].bestAlts[0])
		winner_string := fmt.Sprintf("%d", winner)
		msg, _ := json.Marshal(ballotagent.NewResult_Request{Ballot_Id: winner_string})
		w.Write(msg)
	} else {
		msg, _ := json.Marshal("pas de vainqueur")
		w.Write(msg)
	}

}

func main() {

	fmt.Println("__________________________\n")
	fmt.Println("🗳️ 🗳️   BUREAU DE VOTE 🗳️ 🗳️ ")
	fmt.Println("__________________________\n")

	vs := new(MyServer)
	mux := http.NewServeMux()

	mux.HandleFunc("/new_ballot", vs.NewBallot)
	mux.HandleFunc("/vote", vs.VoteRequest)
	mux.HandleFunc("/result", vs.ResultRequest)

	s := &http.Server{
		Addr:           "localhost:8080",
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20}
	log.Println("Listening on localhost:8080")
	log.Fatal(s.ListenAndServe())
}
