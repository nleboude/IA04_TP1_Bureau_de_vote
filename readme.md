Pour lancer le bureau de vote, il suffit de faire, depuis le dossier, 
```go
go run .
```

Ce programme permet d'implémenter un bureau de vote. Il est possible d'envoyer les commandes /new_ballot, /vote et /result.
/new_ballot crée un scrutin. Les paramètres possibles sont les agents pouvant voter, la méthode de vote et le nombre d'alternatives.

![start](./Images/new_ballot.png)

/vote permet à un agent de donner ses préférences.

![start](./Images/vote.png)

/result permet d'obtenir le résultat du scrutin.

![start](./Images/result.png)

Les méthodes de vote Majoritaire ("Majority"), de Borda("Borda"), par Approbation ("Approval"), de Condorcet ("Condorcet") et de Copeland ("Copeland) sont disponibles.
